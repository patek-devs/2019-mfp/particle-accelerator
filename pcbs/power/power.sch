EESchema Schematic File Version 4
LIBS:Cern2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:CP C3
U 1 1 5CE164EB
P 6200 4700
F 0 "C3" H 6318 4746 50  0000 L CNN
F 1 "3G3" H 6318 4655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 6238 4550 50  0001 C CNN
F 3 "~" H 6200 4700 50  0001 C CNN
	1    6200 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery J2
U 1 1 5CE1665C
P 7550 4550
F 0 "J2" H 7658 4596 50  0000 L CNN
F 1 "INPUT-28V" H 7658 4505 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2-5.08_1x02_P5.08mm_Horizontal" V 7550 4610 50  0001 C CNN
F 3 "~" V 7550 4610 50  0001 C CNN
	1    7550 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5CE18CA1
P 5750 4700
F 0 "C2" H 5868 4746 50  0000 L CNN
F 1 "3G3" H 5868 4655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 5788 4550 50  0001 C CNN
F 3 "~" H 5750 4700 50  0001 C CNN
	1    5750 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 5CE18CDD
P 6650 4700
F 0 "C5" H 6768 4746 50  0000 L CNN
F 1 "3G3" H 6768 4655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 6688 4550 50  0001 C CNN
F 3 "~" H 6650 4700 50  0001 C CNN
	1    6650 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C6
U 1 1 5CE18D15
P 7100 4700
F 0 "C6" H 7218 4746 50  0000 L CNN
F 1 "3G3" H 7218 4655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 7138 4550 50  0001 C CNN
F 3 "~" H 7100 4700 50  0001 C CNN
	1    7100 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C7
U 1 1 5CE18D4F
P 6650 3950
F 0 "C7" H 6768 3996 50  0000 L CNN
F 1 "3G3" H 6768 3905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 6688 3800 50  0001 C CNN
F 3 "~" H 6650 3950 50  0001 C CNN
	1    6650 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 4750 7550 5150
$Comp
L Device:R R4
U 1 1 5CE19C50
P 7350 3500
F 0 "R4" V 7143 3500 50  0000 C CNN
F 1 "33" V 7234 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_Power_L48.0mm_W12.5mm_P55.88mm" V 7280 3500 50  0001 C CNN
F 3 "~" H 7350 3500 50  0001 C CNN
	1    7350 3500
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5CE1C226
P 5750 3150
F 0 "C4" H 5865 3196 50  0000 L CNN
F 1 "100n" H 5865 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 5788 3000 50  0001 C CNN
F 3 "~" H 5750 3150 50  0001 C CNN
	1    5750 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5CE1C282
P 7100 3150
F 0 "C8" H 7215 3196 50  0000 L CNN
F 1 "100n" H 7215 3105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 7138 3000 50  0001 C CNN
F 3 "~" H 7100 3150 50  0001 C CNN
	1    7100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2900 7550 3500
Wire Wire Line
	5550 3300 5750 3300
Text Notes 5850 2500 0    98   ~ 0
Zdroj 15V a 28V
$Comp
L Device:CP C9
U 1 1 5CE4FDA5
P 5750 3950
F 0 "C9" H 5868 3996 50  0000 L CNN
F 1 "3G3" H 5868 3905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 5788 3800 50  0001 C CNN
F 3 "~" H 5750 3950 50  0001 C CNN
	1    5750 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C10
U 1 1 5CE4FDF5
P 6200 3950
F 0 "C10" H 6318 3996 50  0000 L CNN
F 1 "3G3" H 6318 3905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 6238 3800 50  0001 C CNN
F 3 "~" H 6200 3950 50  0001 C CNN
	1    6200 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C11
U 1 1 5CE4FF14
P 7100 3950
F 0 "C11" H 7218 3996 50  0000 L CNN
F 1 "3G3" H 7218 3905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D18.0mm_P7.50mm" H 7138 3800 50  0001 C CNN
F 3 "~" H 7100 3950 50  0001 C CNN
	1    7100 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 3500 7550 3500
Connection ~ 7550 3500
Connection ~ 5750 3500
Wire Wire Line
	5750 3500 6200 3500
Wire Wire Line
	6200 3500 6200 3800
Connection ~ 6200 3500
Wire Wire Line
	6650 3500 6650 3800
Wire Wire Line
	6200 3500 6650 3500
Connection ~ 6650 3500
Wire Wire Line
	6650 3500 7100 3500
Wire Wire Line
	7100 3500 7100 3800
Connection ~ 7100 3500
Wire Wire Line
	7100 3500 7200 3500
Wire Wire Line
	5750 4100 6200 4100
Connection ~ 6200 4100
Wire Wire Line
	6200 4100 6650 4100
Connection ~ 6650 4100
Wire Wire Line
	6650 4100 7100 4100
Wire Wire Line
	7550 3500 7550 4350
Wire Wire Line
	5750 4850 5750 5150
Wire Wire Line
	5750 5150 6200 5150
Wire Wire Line
	6200 4850 6200 5150
Connection ~ 6200 5150
Wire Wire Line
	6200 5150 6650 5150
Wire Wire Line
	6650 4850 6650 5150
Connection ~ 6650 5150
Wire Wire Line
	6650 5150 7100 5150
Wire Wire Line
	7100 4850 7100 5150
Connection ~ 7100 5150
Wire Wire Line
	7100 5150 7550 5150
Wire Wire Line
	5750 4550 5750 4450
Wire Wire Line
	5750 4450 6200 4450
Wire Wire Line
	7100 4450 7100 4550
Wire Wire Line
	6650 4550 6650 4450
Connection ~ 6650 4450
Wire Wire Line
	6650 4450 7100 4450
Wire Wire Line
	6200 4450 6200 4550
Connection ~ 6200 4450
Wire Wire Line
	6200 4450 6650 4450
Wire Wire Line
	5750 4100 5550 4100
Connection ~ 5750 4100
Wire Wire Line
	5750 4450 5350 4450
Connection ~ 5750 4450
Connection ~ 5750 3300
Wire Wire Line
	5750 3300 6450 3300
Wire Wire Line
	6750 2900 7100 2900
Wire Wire Line
	7100 3000 7100 2900
Connection ~ 7100 2900
Wire Wire Line
	7100 2900 7550 2900
Wire Wire Line
	5750 3000 5750 2900
Wire Wire Line
	5750 2900 6150 2900
Wire Wire Line
	6450 3200 6450 3300
Connection ~ 6450 3300
Wire Wire Line
	6450 3300 7100 3300
Text Notes 8550 7500 0    98   ~ 0
Patek Cern 2
Text Notes 8750 7650 0    59   ~ 0
23.5.2019
$Comp
L Regulator_Linear:L7815 U1
U 1 1 5CE1AA3D
P 6450 2900
F 0 "U1" H 6450 3142 50  0000 C CNN
F 1 "78S15" H 6450 3051 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6475 2750 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 6450 2850 50  0001 C CNN
	1    6450 2900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5CED097C
P 3600 3900
F 0 "J3" H 3518 4217 50  0000 C CNN
F 1 "OUTPUT-28V" H 3518 4126 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-4_1x04_P5.00mm_Horizontal" H 3600 3900 50  0001 C CNN
F 3 "~" H 3600 3900 50  0001 C CNN
	1    3600 3900
	-1   0    0    -1  
$EndComp
Connection ~ 5550 4100
Connection ~ 5750 5150
Wire Wire Line
	5750 3800 5750 3500
Wire Wire Line
	5550 3300 5550 4100
Wire Wire Line
	5550 5150 5750 5150
Wire Wire Line
	5350 3500 5750 3500
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 5CE82363
P 3600 3200
F 0 "J6" H 3518 3517 50  0000 C CNN
F 1 "OUTPUT-15V" H 3518 3426 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-4_1x04_P5.00mm_Horizontal" H 3600 3200 50  0001 C CNN
F 3 "~" H 3600 3200 50  0001 C CNN
	1    3600 3200
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5CE83255
P 3600 4650
F 0 "J5" H 3518 4967 50  0000 C CNN
F 1 "OUTPUT-GND" H 3518 4876 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-4_1x04_P5.00mm_Horizontal" H 3600 4650 50  0001 C CNN
F 3 "~" H 3600 4650 50  0001 C CNN
	1    3600 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 2900 3800 2900
Wire Wire Line
	3800 2900 3800 3100
Connection ~ 5750 2900
Wire Wire Line
	3800 3200 3800 3100
Connection ~ 3800 3100
Wire Wire Line
	3800 3300 3800 3200
Connection ~ 3800 3200
Wire Wire Line
	3800 3400 3800 3300
Connection ~ 3800 3300
Wire Wire Line
	3800 4100 3800 4000
Wire Wire Line
	3800 3900 3800 4000
Connection ~ 3800 4000
Wire Wire Line
	3800 3900 3800 3800
Connection ~ 3800 3900
Wire Wire Line
	3800 3800 5350 3800
Wire Wire Line
	5350 3500 5350 3800
Connection ~ 3800 3800
Connection ~ 5350 3800
Wire Wire Line
	5350 3800 5350 4450
Wire Wire Line
	3800 4850 3800 4750
Wire Wire Line
	3800 4750 3800 4650
Connection ~ 3800 4750
Wire Wire Line
	3800 4650 3800 4550
Connection ~ 3800 4650
Wire Wire Line
	3800 4550 5550 4550
Connection ~ 3800 4550
Wire Wire Line
	5550 4100 5550 4550
Wire Wire Line
	5550 4550 5550 5150
Connection ~ 5550 4550
$EndSCHEMATC
