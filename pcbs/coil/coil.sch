EESchema Schematic File Version 4
LIBS:Cern2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2450 850  0    98   ~ 0
Spínání cívky
Text Notes 8550 7500 0    98   ~ 0
Patek Cern 2
Text Notes 8750 7650 0    59   ~ 0
23.5.2019
Wire Wire Line
	3150 1350 3150 2150
Wire Wire Line
	2650 1350 3150 1350
Wire Wire Line
	2650 2100 2650 1350
Wire Wire Line
	2650 2500 2650 3000
Wire Wire Line
	2350 1350 2350 1700
Wire Wire Line
	2650 3000 2800 3000
Connection ~ 2650 1350
Wire Wire Line
	2350 1350 2650 1350
Connection ~ 2800 3000
Wire Wire Line
	2800 2800 2800 3000
Wire Wire Line
	3100 3300 4000 3300
Wire Wire Line
	3100 3050 3600 3050
Wire Wire Line
	3250 2800 3100 2800
Wire Wire Line
	3550 3000 3550 3600
Wire Wire Line
	3550 3600 3900 3600
Connection ~ 3900 3600
Wire Wire Line
	3900 3250 3900 3600
Wire Wire Line
	3900 3600 4300 3600
Connection ~ 4300 3600
Wire Wire Line
	4300 3500 4300 3600
Wire Wire Line
	4300 2550 4300 3100
Wire Wire Line
	3900 2550 4300 2550
Wire Wire Line
	3900 2550 3900 2850
Connection ~ 3900 2550
Wire Wire Line
	3550 2550 3900 2550
Connection ~ 3550 3600
Wire Wire Line
	3150 3600 3550 3600
Wire Wire Line
	3150 2450 3150 3600
$Comp
L Device:R R6
U 1 1 5CE33135
P 2950 3300
F 0 "R6" V 2800 3300 50  0000 C CNN
F 1 "47" V 2850 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2880 3300 50  0001 C CNN
F 3 "~" H 2950 3300 50  0001 C CNN
	1    2950 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 3000 2800 3050
Wire Wire Line
	2800 3050 2800 3300
Connection ~ 2800 3050
$Comp
L Device:R R5
U 1 1 5CE330F1
P 2950 3050
F 0 "R5" V 2800 3050 50  0000 C CNN
F 1 "47" V 2850 3050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2880 3050 50  0001 C CNN
F 3 "~" H 2950 3050 50  0001 C CNN
	1    2950 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 2550 3550 2600
$Comp
L Transistor_FET:IRF3205 Q5
U 1 1 5CE302CF
P 4200 3300
F 0 "Q5" H 4406 3346 50  0000 L CNN
F 1 "IRF3205" H 4150 2800 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4450 3225 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf3205.pdf" H 4200 3300 50  0001 L CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF3205 Q4
U 1 1 5CE3026B
P 3800 3050
F 0 "Q4" H 4006 3096 50  0000 L CNN
F 1 "IRF3205" H 3750 2300 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 4050 2975 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf3205.pdf" H 3800 3050 50  0001 L CNN
	1    3800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3300 1750 3300
Wire Wire Line
	1650 3600 2350 3600
Wire Wire Line
	1650 3500 1650 3600
Wire Wire Line
	1550 3500 1650 3500
$Comp
L Connector:Jack-DC J1
U 1 1 5CE1F4C9
P 1250 3400
F 0 "J1" H 1305 3725 50  0000 C CNN
F 1 "Vstup pulzu" H 1305 3634 50  0000 C CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x02_P1.00mm_Vertical" H 1300 3360 50  0001 C CNN
F 3 "~" H 1300 3360 50  0001 C CNN
	1    1250 3400
	1    0    0    -1  
$EndComp
Connection ~ 3550 2550
Wire Wire Line
	3550 2400 3550 2550
Wire Wire Line
	3800 2400 3550 2400
Wire Wire Line
	3800 2300 3800 2400
Connection ~ 3550 2400
Wire Wire Line
	3550 2300 3550 2400
Wire Wire Line
	3800 2000 3800 1950
Wire Wire Line
	3550 1950 3800 1950
Wire Wire Line
	3550 2000 3550 1950
$Comp
L Device:D D2
U 1 1 5CE16473
P 3800 2150
F 0 "D2" V 3700 2250 50  0000 L CNN
F 1 "P600" V 3800 2300 50  0000 L CNN
F 2 "Diode_THT:D_P600_R-6_P20.00mm_Horizontal" H 3800 2150 50  0001 C CNN
F 3 "~" H 3800 2150 50  0001 C CNN
	1    3800 2150
	0    1    1    0   
$EndComp
$Comp
L Device:L L1
U 1 1 5CE1633B
P 3550 2150
F 0 "L1" H 3603 2196 50  0000 L CNN
F 1 "Coil" H 3500 2400 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2_1x02_P5.00mm_Horizontal" H 3550 2150 50  0001 C CNN
F 3 "~" H 3550 2150 50  0001 C CNN
	1    3550 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3600 2350 3500
Connection ~ 2350 3600
Connection ~ 3150 3600
Wire Wire Line
	3150 3600 2350 3600
$Comp
L Transistor_FET:IRF3205 Q3
U 1 1 5CE160A9
P 3450 2800
F 0 "Q3" H 3656 2846 50  0000 L CNN
F 1 "IRF3205" H 3350 1800 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3700 2725 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf3205.pdf" H 3450 2800 50  0001 L CNN
	1    3450 2800
	1    0    0    -1  
$EndComp
Connection ~ 2650 3000
Wire Wire Line
	2350 3000 2350 3100
Connection ~ 2350 3000
$Comp
L Device:D D1
U 1 1 5CE15ECA
P 2500 3000
F 0 "D1" H 2500 3216 50  0000 C CNN
F 1 "1n4148" H 2500 3125 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P7.62mm_Horizontal" H 2500 3000 50  0001 C CNN
F 3 "~" H 2500 3000 50  0001 C CNN
	1    2500 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5CE15E30
P 3150 2300
F 0 "C1" H 3265 2346 50  0000 L CNN
F 1 "100n" H 3265 2255 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3188 2150 50  0001 C CNN
F 3 "~" H 3150 2300 50  0001 C CNN
	1    3150 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 2300 2350 3000
Wire Wire Line
	2350 2000 2350 2300
Connection ~ 2350 2300
$Comp
L Transistor_BJT:BC337 Q2
U 1 1 5CE15D83
P 2550 2300
F 0 "Q2" H 2741 2346 50  0000 L CNN
F 1 "BC337" H 2700 2600 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 2750 2225 50  0001 L CIN
F 3 "http://www.nxp.com/documents/data_sheet/BC817_BC817W_BC337.pdf" H 2550 2300 50  0001 L CNN
	1    2550 2300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC337 Q1
U 1 1 5CE15D35
P 2250 3300
F 0 "Q1" H 2441 3346 50  0000 L CNN
F 1 "BC337" H 2441 3255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 2450 3225 50  0001 L CIN
F 3 "http://www.nxp.com/documents/data_sheet/BC817_BC817W_BC337.pdf" H 2250 3300 50  0001 L CNN
	1    2250 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5CE15C61
P 2950 2800
F 0 "R3" V 2800 2800 50  0000 C CNN
F 1 "47" V 2850 2800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2880 2800 50  0001 C CNN
F 3 "~" H 2950 2800 50  0001 C CNN
	1    2950 2800
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5CE15C0E
P 1900 3300
F 0 "R1" V 1693 3300 50  0000 C CNN
F 1 "1k" V 1784 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1830 3300 50  0001 C CNN
F 3 "~" H 1900 3300 50  0001 C CNN
	1    1900 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5CE15B50
P 2350 1850
F 0 "R2" H 2420 1896 50  0000 L CNN
F 1 "2k2" H 2420 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2280 1850 50  0001 C CNN
F 3 "~" H 2350 1850 50  0001 C CNN
	1    2350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3600 5200 2450
Wire Wire Line
	4300 3600 5200 3600
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 5CEFC795
P 5400 2350
F 0 "J4" H 5318 2667 50  0000 C CNN
F 1 "INPUT" H 5318 2576 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-3_1x03_P5.00mm_Horizontal" H 5400 2350 50  0001 C CNN
F 3 "~" H 5400 2350 50  0001 C CNN
	1    5400 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1350 4950 1350
Wire Wire Line
	4950 1350 4950 2350
Wire Wire Line
	4950 2350 5200 2350
Connection ~ 3150 1350
Wire Wire Line
	3800 1950 5050 1950
Wire Wire Line
	5050 1950 5050 2250
Wire Wire Line
	5050 2250 5200 2250
Connection ~ 3800 1950
$EndSCHEMATC
