module gitlab.com/patek-devs/2019-mfp/particle-accelerator/plotting-go

go 1.16

require (
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
)
